import SockJS from 'sockjs-client';
import Stomp from 'stompjs'

export interface StompListener {
    onMessage(s:Subscription, payload:string):void
    onError(s:Subscription):void
    synchronize(s:Subscription):void
}

export class Subscription {
    constructor(public persistent:boolean,
                public idString:string,
                public destination:string) {
    }
}

export class StompSubscriptionClient {
    private client:Stomp.Client
    private isConnected:boolean = false
    private existingSubscriptions:Map<Subscription, Stomp.Subscription> = new Map()
    private subscriptionListeners:Map<Subscription, StompListener> = new Map()
    private pendingSubscriptions:Subscription[] = []
    private subscriptionCounter:number = 1
    private pushVhost:string
    private allowedFailures:number
    private failures:number = 0
    private onPersistentFailures:() => void
    private needToResubscribe:boolean = false
    private wsUrl:string


    constructor(wsURL:string, pushVhost:string, allowedFailures:number, onPersistentFailures:() => void) {
        this.wsUrl = wsURL
        this.pushVhost = pushVhost
        this.allowedFailures = allowedFailures
        this.onPersistentFailures = onPersistentFailures
        this.client = Stomp.over(SockJS(wsURL))
        this.connect()
    }

    connect() {
        if (!this.isConnected) {
            this.client = Stomp.over(SockJS(this.wsUrl))
            this.client.connect({
                login: "",
                passcode: ""
            }, () => {
                this.onConnected()
            }, () => {
                this.onError()
            })
        }
    }

    subscribe(userLogin:string, persistent:boolean, destination:string, listener:StompListener):Subscription {
        let login = `${userLogin}#${new Date().getTime()}#${this.subscriptionCounter}`
        this.subscriptionCounter += 1
        let subscription = new Subscription(persistent, login, destination)
        this.subscriptionListeners.set(subscription, listener)
        if (!this.isConnected) {
            this.pendingSubscriptions.push(subscription)
        } else {
            this.internalSubscribe(subscription)
        }
        return subscription
    }

    unsubscribe(s:Subscription) {
        let subscription = this.existingSubscriptions.get(s)
        this.existingSubscriptions.delete(s)
        if (subscription) {
            subscription.unsubscribe()
        }
    }

    private findSubscriptionListener(s:Subscription):StompListener|undefined {
        return this.subscriptionListeners.get(s)
    }

    private onConnected() {
        this.isConnected = true
        this.applyPendingSubscriptions()
        if (this.needToResubscribe) {
            this.needToResubscribe = false
            this.reapplyExistingSubscriptions()
        }
    }

    private onError() {
        this.isConnected = false
        this.needToResubscribe = true
        for (let s of this.existingSubscriptions.keys()) {
            let listener = this.findSubscriptionListener(s)
            if (listener) {
                listener.onError(s)
            }
        }
        this.failures += 1
        setTimeout(() => {
            this.connect()
        }, 300)
    }

    private applyPendingSubscriptions() {
        for (var pendingSubscription of this.pendingSubscriptions) {
            this.internalSubscribe(pendingSubscription)
        }
        this.pendingSubscriptions = []
    }

    private reapplyExistingSubscriptions() {
        var subscriptionHolder = new Map<Subscription, Stomp.Subscription>(this.existingSubscriptions)
        this.existingSubscriptions.clear()
        for (var [key,value] of subscriptionHolder.entries()) {
            value.unsubscribe()
            this.internalSubscribe(key)
        }
    }

    private internalSubscribe(s:Subscription) {
        var headers: {[key: string]: string} = {}
        headers['id'] = s.idString
        headers['ack'] = "client-individual"

        let maybeSubscriptionListener = this.findSubscriptionListener(s)
        if (maybeSubscriptionListener) {
            let subscriptionListener = maybeSubscriptionListener
            let stompSubscription = this.client.subscribe(
                s.destination,
                (f: Stomp.Message) => {
                    f.ack();
                    subscriptionListener.onMessage(s, f.body)
                }, headers)
            this.existingSubscriptions.set(s, stompSubscription)

            subscriptionListener.synchronize(s)
        }
        return s
    }

}
