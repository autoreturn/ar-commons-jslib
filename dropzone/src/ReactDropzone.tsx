import * as React from 'react'
import * as ReactDOM from 'react-dom'
import Dropzone from 'dropzone';
import classnames from 'classnames'
import './dropzone.css'
import './filepicker.css'


type VoidCallback = () => any;
type InitCallback = (dropzone: Dropzone) => any;
type DragEventCallback = (event: DragEvent) => any;
type FileCallback = (file: Dropzone.DropzoneFile) => any;
type FileArrayCallback = (files: Dropzone.DropzoneFile[]) => any;
type ThumbnailCallback = (file: Dropzone.DropzoneFile, dataUrl: string) => any;
type ErrorCallback = (file: Dropzone.DropzoneFile, message: string | Error) => any;
type ErrorMultipleCallback = (files: Dropzone.DropzoneFile[], message: string | Error) => any;
type UploadProgressCallback = (file: Dropzone.DropzoneFile, progress: number, bytesSent: number) => any;
type TotalUploadProgressCallback = (totalProgress: number, totalBytes: number, totalBytesSent: number) => any;
type SendingCallback = (file: Dropzone.DropzoneFile, xhr: XMLHttpRequest, formData: FormData) => any;
type SendingMultipleCallback = (files: Dropzone.DropzoneFile[], xhr: XMLHttpRequest, formData: FormData) => any;
type SuccessCallback = (file: Dropzone.DropzoneFile, response: Object | string) => any;
type SuccessMultipleCallback = (files: Dropzone.DropzoneFile[], responseText: string) => any;

/* handlers based on ts definitions for Dropzone.js (@types/dropzone) */
export declare interface DropzoneComponentHandlers {
    init?: InitCallback | InitCallback[];

    // All of these receive the event as first parameter:
    drop?: DragEventCallback | DragEventCallback[];
    dragstart?: DragEventCallback | DragEventCallback[];
    dragend?: DragEventCallback | DragEventCallback[];
    dragenter?: DragEventCallback | DragEventCallback[];
    dragover?: DragEventCallback | DragEventCallback[];
    dragleave?: DragEventCallback | DragEventCallback[];
    paste?: DragEventCallback | DragEventCallback[];

    reset?: VoidCallback | VoidCallback[];

    addedfile?: FileCallback | FileCallback[];
    addedfiles?: FileArrayCallback | FileArrayCallback[];
    removedfile?: FileCallback | FileCallback[];
    thumbnail?: ThumbnailCallback | ThumbnailCallback[];

    error?: ErrorCallback | ErrorCallback[];
    errormultiple?: ErrorMultipleCallback | ErrorMultipleCallback[];

    processing?: FileCallback | FileCallback[];
    processingmultiple?: FileArrayCallback | FileArrayCallback[];

    uploadprogress?: UploadProgressCallback | UploadProgressCallback[];
    totaluploadprogress?: TotalUploadProgressCallback | TotalUploadProgressCallback[];

    sending?: SendingCallback | SendingCallback[];
    sendingmultiple?: SendingMultipleCallback | SendingMultipleCallback[];

    success?: SuccessCallback | SuccessCallback[];
    successmultiple?: SuccessMultipleCallback | SuccessMultipleCallback[];

    canceled?: FileCallback | FileCallback[];
    canceledmultiple?: FileArrayCallback | FileArrayCallback[];

    complete?: FileCallback | FileCallback[];
    completemultiple?: FileArrayCallback | FileArrayCallback[];

    maxfilesexceeded?: FileCallback | FileCallback[];
    maxfilesreached?: FileArrayCallback | FileArrayCallback[];

    queuecomplete?: VoidCallback | VoidCallback[];
}

export declare interface DropzoneComponentConfig {
    showFiletypeIcon?: boolean;
    iconFiletypes?: string[];
    postUrl?: string;
    dropzoneSelector?: string;
}

interface DropzoneComponentProps {
    enable: boolean,
    djsConfig?: Dropzone.DropzoneOptions;
    config: DropzoneComponentConfig;
    eventHandlers?: DropzoneComponentHandlers;
    className?: string;
    action?: string;
    hideMessageAfterUpload?: boolean
}

interface DropzoneState {
    files: any
}


export class DropzoneComponent extends React.Component<DropzoneComponentProps, DropzoneState> {

    dropzone: any;
    queueDestroy: boolean | undefined;

    constructor(props: DropzoneComponentProps) {
        super(props);

        this.state = {files: []}
    }

    /**
     * Configuration of Dropzone.js. Defaults are
     * overriden by the 'djsConfig' property
     * For a full list of possible configurations,
     * please consult
     * http://www.dropzonejs.com/#configuration
     */
    getDjsConfig() {
        let options: Dropzone.DropzoneOptions;
        const defaults = {
            url: this.props.config.postUrl
        };

        if (this.props.djsConfig) {
            // options = extend(true, {}, defaults, this.props.djsConfig);
            options = {...defaults, ...this.props.djsConfig}
        } else {
            options = defaults
        }

        return options
    }

    /**
     * React 'componentDidMount' method
     * Sets up dropzone.js with the component.
     */
    componentDidMount() {
        const options = this.getDjsConfig();

        Dropzone.autoDiscover = false;

        if (!this.props.config.postUrl && (!this.props.eventHandlers || !this.props.eventHandlers.drop)) {
            console.info('Neither postUrl nor a "drop" eventHandler specified, the React-Dropzone component might misbehave.')
        }

        let dropzoneNode = this.props.config.dropzoneSelector || ReactDOM.findDOMNode(this);
        this.dropzone = new Dropzone(dropzoneNode as string | HTMLElement, options);
        this.setupEvents();
    }

    /**
     * React 'componentWillUnmount'
     * Removes dropzone.js (and all its globals) if the component is being unmounted
     */
    componentWillUnmount() {
        if (this.dropzone) {
            const files = this.dropzone!.getActiveFiles();

            if (files.length > 0) {
                // Well, seems like we still have stuff uploading.
                // This is dirty, but let's keep trying to get rid
                // of the dropzone until we're done here.
                this.queueDestroy = true;

                const destroyInterval = window.setInterval(() => {
                    if (this.queueDestroy === false) {
                        return window.clearInterval(destroyInterval)
                    }

                    if (this.dropzone!.getActiveFiles().length === 0) {
                        this.dropzone = this.destroy(this.dropzone);
                        return window.clearInterval(destroyInterval)
                    }
                }, 500)
            } else {
                this.dropzone = this.destroy(this.dropzone)
            }
        }
    }

    /**
     * React 'componentDidUpdate'
     * If the Dropzone hasn't been created, create it
     */
    componentDidUpdate() {
        this.queueDestroy = false;

        if (!this.dropzone) {
            const dropzoneNode = this.props.config.dropzoneSelector || ReactDOM.findDOMNode(this);
            this.dropzone = new Dropzone(dropzoneNode as string | HTMLElement, this.getDjsConfig())
        }

        if (this.props.enable) {
            this.dropzone.enable();
        } else {
            this.dropzone.disable();
        }
    }

    /**
     * React 'componentWillUpdate'
     * Update Dropzone options each time the component updates.
     */
    componentWillUpdate() {
        let djsConfigObj: any;
        let postUrlConfigObj: any;

        djsConfigObj = this.props.djsConfig ? this.props.djsConfig : {};

        try {
            postUrlConfigObj = this.props.config.postUrl ? {url: this.props.config.postUrl} : {}
        } catch (err) {
            postUrlConfigObj = {}
        }

        this.dropzone.options = {...this.dropzone.options, ...djsConfigObj, ...postUrlConfigObj}
    }

    /**
     * React 'render'
     */
    render() {
        const {files} = this.state;
        const {enable, config} = this.props;
        const className = classnames(
            'filepicker dropzone',
            this.props.className,
            {'dz-message-hide-after-upload': this.props.hideMessageAfterUpload},
            enable ? '' : 'disabled'
        );


        if (!config.postUrl && this.props.action) {
            return (
                <form action={this.props.action} className={className}>
                    {this.props.children}
                </form>
            )
        } else {
            return (
                <div className={className}>{this.props.children} </div>
            )
        }
    }

    /**
     * Takes event handlers in this.props.eventHandlers
     * and binds them to dropzone.js events
     */
    setupEvents() {
        const eventHandlers: any = this.props.eventHandlers;

        if (!this.dropzone || !eventHandlers) return;

        for (let eventHandler in eventHandlers) {
            if (eventHandlers.hasOwnProperty(eventHandler) && eventHandlers[eventHandler]) {

                let eventHandlerInstance = eventHandlers[eventHandler];
                // Check if there's an array of event handlers
                if (Object.prototype.toString.call(eventHandlerInstance) === '[object Array]') {
                    for (let i = 0; i < eventHandlerInstance.length; i = i + 1) {
                        // Check if it's an init handler
                        if (eventHandler === 'init') {
                            eventHandlerInstance[i](this.dropzone)
                        } else {
                            this.dropzone!.on(eventHandler, eventHandlers[eventHandler][i])
                        }
                    }
                } else {
                    if (eventHandler === 'init') {
                        (eventHandlers[eventHandler] as any)(this.dropzone)
                    } else {
                        this.dropzone!.on(eventHandler, eventHandlers[eventHandler])
                    }
                }
            }
        }

        this.dropzone!.on('addedfile', (file: any) => {
            if (!file) return;

            const files = this.state.files || [];

            files.push(file);
            this.setState({files})
        });

        this.dropzone!.on('removedfile', (file: any) => {
            if (!file) return;

            const files = this.state.files || [];
            files.forEach((fileInFiles: any, i: number) => {
                if (fileInFiles.name === file.name && fileInFiles.size === file.size) {
                    files.splice(i, 1)
                }
            });

            this.setState({files})
        })
    }

    /**
     * Removes ALL listeners and Destroys dropzone. see https://github.com/enyo/dropzone/issues/1175
     */
    destroy(dropzone: any) {
        dropzone.off();
        return dropzone.destroy()
    }

    processQueue() {
        this.dropzone.processQueue();
    }
}

