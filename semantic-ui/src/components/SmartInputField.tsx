import React, {FC} from "react";
import {Form, Input, InputProps} from "semantic-ui-react";
import {observer} from "mobx-react-lite";
import ErrorPopup from "./ErrorPopup";
import {IInputFieldModel} from "../models/InputFieldModel";


interface SmartInputFieldProps extends InputProps {
    model: IInputFieldModel
    fieldLabel?:string
}


//the SmartInputField is shorthand for an input field that uses IInputFieldModel
//it is not necessary to use this if you want more customization.
const SmartInputField: FC<SmartInputFieldProps> = (props: SmartInputFieldProps) => {
    const {label, required, model, fluid, disabled, width, fieldLabel, ...rest} = props;
    return (
        <ErrorPopup position="bottom left" content={disabled ? null : model.message}>
            <Form.Field error={!!model.message} required={required} width={width}>
                <label>{label}</label>
                <Input
                    label={fieldLabel}
                    fluid={fluid}
                    onFocus={model.onFocus}
                    onBlur={model.onBlur}
                    onChange={model.onChange}
                    value={model.value}
                    disabled={disabled}
                    {...rest}
                />
            </Form.Field>
        </ErrorPopup>
    );
};

// Specifies the default values for props:
SmartInputField.defaultProps = {
    fluid: true,
    className: "padded"
};

export default observer(SmartInputField);