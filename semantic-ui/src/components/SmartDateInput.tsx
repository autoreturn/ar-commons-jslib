import React, {FC} from "react";
import {DateInput, DateTimeInputProps} from "semantic-ui-calendar-react";
import {observer} from "mobx-react-lite";
import {Form} from "semantic-ui-react";
import ErrorPopup from "./ErrorPopup";
import {IInputFieldModel} from "../models/InputFieldModel";

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

type SmartDateFieldProps = Omit<DateTimeInputProps, "onChange" | "value">;

interface SmartDateInputProps extends SmartDateFieldProps {
    model: IInputFieldModel
}

const SmartDateInput: FC<SmartDateInputProps> = (props: SmartDateInputProps) => {
    const {label, required, disabled, model, as = DateInput, ...rest} = props;

    const DateComponent = as;

    return (
        <ErrorPopup position="bottom left" content={disabled ? null : model.message}>
            <Form.Field error={!!model.message} required={required} >
                <label>{label}</label>
                <DateComponent
                    {...rest}
                    disabled={Boolean(disabled)}
                    animation='fade'
                    onFocus={model.onFocus}
                    onBlur={model.onBlur}
                    onChange={model.onChange}
                    value={model.value || ''}
                />
            </Form.Field>
        </ErrorPopup>
    );
};

export default observer(SmartDateInput);