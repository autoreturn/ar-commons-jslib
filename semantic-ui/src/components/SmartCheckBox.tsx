import React, {FC} from "react";
import {FormCheckbox, FormCheckboxProps} from "semantic-ui-react";
import {observer} from "mobx-react-lite";
import ErrorPopup from "./ErrorPopup";
import {ICheckboxModel} from "../models/CheckboxModel"

interface SmartCheckBoxProps extends FormCheckboxProps {
    model: ICheckboxModel
}

const SmartCheckBox: FC<SmartCheckBoxProps> = (props: SmartCheckBoxProps) => {
    const {model, disabled, ...rest} = props;
    return (
        <ErrorPopup position="bottom left" content={disabled ? null : model.message}>
            <FormCheckbox
                {...rest}
                error={!!model.message}
                checked={model.checked}
                onFocus={model.onFocus}
                onBlur={model.onBlur}
                onChange={model.onChange}/>
        </ErrorPopup>
    );
};

export default observer(SmartCheckBox);