import React, {FC} from "react";
import {FormTextArea, StrictFormTextAreaProps} from "semantic-ui-react";
import {observer} from "mobx-react-lite";
import ErrorPopup from "./ErrorPopup";
import {ITextAreaModel} from "../models/InputFieldModel";


interface SmartTextAreaProps extends StrictFormTextAreaProps {
    model: ITextAreaModel
}


//the SmartTextArea is shorthand for an input field that uses IInputFieldModel
//it is not necessary to use this if you want more customization.
const SmartTextArea: FC<SmartTextAreaProps> = (props: SmartTextAreaProps) => {
    const {model, disabled, ...rest} = props;

    return (
        <ErrorPopup position="bottom left" content={disabled ? null : model.message}>
            <FormTextArea
                {...rest}
                disabled={disabled}
                error={!!model.message}
                onInput={model.onInput}
                onFocus={model.onFocus}
                onBlur={model.onBlur}
                value={model.value}
            />
        </ErrorPopup>
    );
};

// Specifies the default values for props:
SmartTextArea.defaultProps = {
};

export default observer(SmartTextArea);