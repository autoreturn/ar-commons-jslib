import {default as React, FC} from "react";
import {
   DropdownItemProps,
   DropdownProps,
   FormSelect,
   StrictFormFieldProps
} from "semantic-ui-react";
import {observer} from "mobx-react-lite";
import ErrorPopup from "./ErrorPopup";
import {IDropdownModel, IDropdownMultiModel, renderToText, renderToValue} from "../models/DropdownModel";
import {isArrayLike} from "mobx";

interface SmartDropdownProps extends StrictFormFieldProps, DropdownProps {
   /** Individual fields may display an error state along with a message. */
   error?: any

   model: IDropdownModel<any> | IDropdownMultiModel<any>
}

const SmartDropdown: FC<SmartDropdownProps> = ({model, disabled, ...props}: SmartDropdownProps) => {
   const item = isArrayLike(model.value) ? model.value.map((item: any) => model.renderItem(item)) : model.renderItem(model.value);
   return (<ErrorPopup position='bottom center' content={disabled ? null : model.message}><FormSelect
       {...props}
       disabled={disabled}
       error={Boolean(model.message)}
       loading={model.status === "loading"}
       onChange={model.onChange}
       onBlur={model.onBlur}
       onFocus={model.onFocus}
       onOpen={model.onDropdownOpen}
       onClose={model.onDropdownClose}
       value={Array.isArray(item) ? item.map((it) => renderToValue(it) || "") : renderToValue(item)}
       text={renderToText(item)}
       renderLabel={(item: DropdownItemProps) => renderToText(item)}
       options={model.options}
   /></ErrorPopup>)
};

SmartDropdown.defaultProps = {
   selection: true,
   search: true,
   selectOnBlur: false
};

const SmartDropdownObserver = observer(SmartDropdown);
export default SmartDropdownObserver;