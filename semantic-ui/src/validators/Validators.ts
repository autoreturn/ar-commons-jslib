export type Validator<T> = (value:T|null|undefined) => string|undefined

export function requiredFieldValidator<T>(fieldName: string,errorMessage?:string|undefined) : Validator<T> {
    return (value => {
        const notEmpty = Array.isArray(value) ? value.length > 0 : Boolean(value);
        if (notEmpty) {
            return undefined
        }
        return errorMessage || `${fieldName} is required`
    })
}

export function regexValidator(fieldName: string, regex: string|undefined, valueRequired:boolean, errorMessage?:string|undefined, ) : Validator<string> {
    return (value => {
        if (!regex) {
            return undefined
        }
        if (!value || value.length === 0) {
            return valueRequired ? errorMessage || `${fieldName} format is invalid` : undefined
        }
        return (new RegExp(regex)).test(value) ? undefined : (errorMessage || `${fieldName} format is invalid`)
    })
}

export function regexOptionalValidator(fieldName: string, regex: string|undefined, errorMessage?:string|undefined) : Validator<string> {
    return regexValidator(fieldName, regex, false, errorMessage)
}

export function regexRequiredValidator(fieldName: string, regex: string|undefined, errorMessage?:string|undefined) : Validator<string> {
    return regexValidator(fieldName, regex, true, errorMessage)
}

export function chainValidator<T>(...validators:Validator<T>[]): Validator<T> {
    return (value => {

        for (let validator of validators) {
            const message = validator(value);
            if (message) {
                return message
            }
        }
        return undefined
    })
}