export interface CodeAndName {
    code: string | number
    name?: string
    displayName?: string
}