import * as React from "react"
import {FocusEventHandler} from "react"
import {InputOnChangeData} from "semantic-ui-react/dist/commonjs/elements/Input/Input"
import {action, autorun, decorate, observable} from "mobx"
import {Validator} from ".."
import {TextAreaProps} from "semantic-ui-react";

export type InputFieldStatus = "normal" | "error"

interface IModel {
    value?: string
    message?: string
    status: InputFieldStatus,
    validate: () => void
}

export interface IInputFieldModel extends IModel{
    onChange: (event: React.SyntheticEvent<HTMLElement>, data: InputOnChangeData) => void
    onBlur: FocusEventHandler<HTMLInputElement>
    onFocus: FocusEventHandler<HTMLInputElement>
}

export interface ITextAreaModel extends IModel {
    onInput: (event: React.FormEvent<HTMLTextAreaElement>, data: TextAreaProps) => void,
    onBlur: FocusEventHandler<HTMLTextAreaElement>
    onFocus: FocusEventHandler<HTMLTextAreaElement>
}

export class InputFieldModel<T extends IModel> {

    status: InputFieldStatus = "normal";
    focused: boolean = false;
    changed: boolean = false;
    message: string | undefined = undefined;
    localValue: string|undefined|null;

    constructor(public validator:Validator<string> = () => undefined, public updateOnChange: boolean = false) {

    }

    shouldInvokeValidator = () => {
        return this.changed && !this.focused;
    };

    validate = (force: boolean = false) => {
        if (this.shouldInvokeValidator() || force) {
            this.message = this.validator(this.localValue || "");
        }
    };

    connect = (x: string |undefined| null | (() => string | null | undefined), onValueChanged: (value: string | undefined, event: React.SyntheticEvent<HTMLElement>) => void): IInputFieldModel => {
        let factory = this;

        autorun(disposer => {
            factory.localValue = typeof x === "function" ? x() : x;
            disposer.dispose()
        });

        let obj = class implements IInputFieldModel {
            get value() {
                return factory.localValue || ""
            }

            get status() {
                return factory.status
            }

            get message() {
                return factory.message;
            }

            validate() {
                factory.validate(true);
            }

            updateValue = (value: string | undefined, event: React.SyntheticEvent<HTMLElement>) => {
                onValueChanged(!value || value === "" ? undefined : value, event)
            };

            onChange: (event: React.SyntheticEvent<HTMLElement>, data: InputOnChangeData) => void = (evt, data) => {
                factory.changed = true;
                factory.localValue = data.value;
                if (factory.updateOnChange) {
                    this.updateValue(data.value, evt)
                }
                factory.validate();
            };

            onFocus: (event: React.SyntheticEvent<HTMLElement>) => void = () => {
                factory.focused = true;
                factory.validate();
            };

            onBlur: FocusEventHandler<HTMLInputElement> = (event => {
                factory.focused = false;
                if (event) {
                    this.updateValue(event.currentTarget.value, event);
                }
                factory.validate();
            })
        };
        return new obj();
    };

    connectTextArea = (x: string |undefined| null, onValueChanged: (value: string | undefined, event: React.SyntheticEvent<HTMLElement>) => void): ITextAreaModel => {
        let factory = this;

        autorun(disposer => {
            factory.localValue = x;
            disposer.dispose()
        });

        let obj = class implements ITextAreaModel {
            get value() {
                return factory.localValue || ""
            }

            get status() {
                return factory.status
            }

            get message() {
                return factory.message;
            }

            validate() {
                factory.validate(true);
            }

            updateValue = (value: string | undefined, event: React.SyntheticEvent<HTMLElement>) => {
                onValueChanged(!value || value === "" ? undefined : value, event)
            };

            onInput: (event: React.SyntheticEvent<HTMLTextAreaElement>, data: TextAreaProps) => void = (evt, data) => {
                factory.changed = true;
                factory.localValue = evt.currentTarget.value;
                if (factory.updateOnChange) {
                    this.updateValue(data.value ? String(data.value) : undefined, evt)
                }
                factory.validate();
            };

            onFocus: (event: React.SyntheticEvent<HTMLTextAreaElement>) => void = () => {
                factory.focused = true;
                factory.validate();
            };

            onBlur: FocusEventHandler<HTMLTextAreaElement> = (event => {
                factory.focused = false;
                this.updateValue(event.currentTarget.value, event);
                factory.validate();
            })
        };
        return new obj();
    }
}

decorate(InputFieldModel, {
    status: observable,
    message: observable,
    localValue: observable,
    connect: action
});
