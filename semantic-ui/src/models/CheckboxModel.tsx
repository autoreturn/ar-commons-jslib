import * as React from "react"
import {FormEvent} from "react"
import {action, decorate, observable} from "mobx"
import {InputFieldStatus} from "./InputFieldModel"
import {CheckboxProps} from "semantic-ui-react"
import {FocusEventHandler} from "react";
import {Validator} from "..";

export interface ICheckboxModel {
    checked?: boolean
    status: InputFieldStatus,
    message?: string,
    validate: () => void
    onChange: (evt: FormEvent<HTMLInputElement>, props: CheckboxProps) => void,
    onBlur: FocusEventHandler<HTMLInputElement>
    onFocus: FocusEventHandler<HTMLInputElement>
}

export class CheckboxModel {

    status: InputFieldStatus = "normal";
    focused: boolean = false;
    changed: boolean = false;
    message: string | undefined = undefined;

    constructor(public validator:Validator<boolean> = () => undefined) {

    }

    shouldInvokeValidator = () => {
        return this.changed && !this.focused;
    };

    validate = (value: boolean | undefined, force: boolean = false) => {
        if (this.shouldInvokeValidator() || force) {
            this.message = this.validator(value);
        }
    };

    connect = (x: boolean | undefined, onValueChanged: (value: boolean | undefined, event: React.SyntheticEvent<HTMLInputElement>) => void): ICheckboxModel => {
        let factory = this;

        let obj = class implements ICheckboxModel {
            get checked() {
                return x
            }

            get status() {
                return factory.status
            }

            onChange: (evt: FormEvent<HTMLInputElement>, props: CheckboxProps) => void = (evt, data) => {
                onValueChanged(data.checked, evt)
            };

            get message() {
                return factory.message;
            }

            validate() {
                factory.validate(x, true);
            }

            onFocus: (event: React.SyntheticEvent<HTMLElement>) => void = () => {
                factory.focused = true;
                factory.validate(x);
            };

            onBlur: FocusEventHandler<HTMLInputElement> = (() => {
                factory.focused = false;
                factory.validate(x);
            })
        };
        return new obj()
    }
}

decorate(CheckboxModel, {
    status: observable,
    message: observable,
    connect: action
})
