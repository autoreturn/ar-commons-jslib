import {DropdownItemProps, DropdownOnSearchChangeData, DropdownProps} from "semantic-ui-react"
import * as React from "react"
import {Validator} from "..";
import {action, computed, decorate, observable} from "mobx";
import {isObservable, toJS} from "mobx"
import {isAlive, isStateTreeNode} from "mobx-state-tree"
import {CodeAndName} from "./CodeAndName";

export type DropdownStatus = "initial" | "loading" | "loaded" | "blocked" | "loading_error" | "error"

/**
 * bridge between DropdownModel status handling and services
 *
 * @param callback
 * @param emptyValue value to add at the top of the available values list
 */
export function withDropdownStatus<T>(callback: (()  => (Promise<T[]> | T[])) | Promise<T[]>, emptyValue?: T | null) : (value?: string) => Promise<[T[], DropdownStatus]> {
    return () => {
        return new Promise<[T[], DropdownStatus]>((resolve, reject) => {
            const result = callback instanceof Promise ? callback : callback();
            const onResult = (res: T[]) => resolve([emptyValue !== undefined ? [emptyValue as T].concat(isStateTreeNode(res) ? res.slice() : res) : res, "loaded" as DropdownStatus]);
            result instanceof Promise
                ? result
                    .then(onResult)
                    .catch(() => reject([[], "error" as DropdownStatus]))

                : onResult(result);
        });
    }
}

export const emptyStatus = [[], 'blocked'];

export function renderCodeAndNameItem(textSource: (v: CodeAndName) => string | undefined = v => typeof v.code === 'number' ? v.code.toString() : v.code): (obj?: CodeAndName) => DropdownItemProps {
    return obj => {
        return obj ? {value: obj.code, text: textSource(obj) || ""} : {value: undefined}
    }
}

export function renderCodeItem(textSource: (v: CodeAndName) => string | undefined = v => typeof v.code === 'number' ? v.code.toString() : v.code): (obj?: CodeAndName) => DropdownItemProps {
    return obj => {
        return obj ? {value: obj.code, text: textSource(obj) || ""} : {value: undefined}
    }
}

export function renderToText(value: DropdownItemProps | undefined) {
    return value && value.text ? value.text.toString() : ""
}

export function renderToValue(value: DropdownItemProps | undefined) {
    return value && value.value
}

export function noSearchFilter(options: DropdownItemProps[], query: string): DropdownItemProps[] {
    return options
}

export interface IDropdownModel<T, ST = T> {
    value: T | undefined | null

    options: (DropdownItemProps & {_obj:ST|null})[]

    message?: string

    errorValue?: ST

    status: DropdownStatus

    renderItem: (t?:ST) => DropdownItemProps

    onChange: (evt: React.SyntheticEvent<HTMLElement>, dropdown: DropdownProps) => void
    onBlur: (evt: React.SyntheticEvent<HTMLElement>, dropdown: DropdownProps) => void
    onFocus: (evt: React.SyntheticEvent<HTMLElement>, dropdown: DropdownProps) => void
    onDropdownOpen?: (event: React.SyntheticEvent<HTMLElement>, data: DropdownProps) => void
    onDropdownClose?: (event: React.SyntheticEvent<HTMLElement>, data: DropdownProps) => void
    onSearchChange?: (evt: React.SyntheticEvent<HTMLElement>, data: DropdownOnSearchChangeData) => void
    searchFilter?: (options: DropdownItemProps[], query: string) => DropdownItemProps[],
    validate: () => void
}

export interface IDropdownMultiModel<T> extends IDropdownModel<T[], T> {
    value: T[],
    onAdd?: (evt:React.SyntheticEvent<HTMLElement>, dropdown: DropdownItemProps) => void
    onRemove?: (evt:React.SyntheticEvent<HTMLElement>, dropdown: DropdownItemProps) => void
}

export interface IDropdownModelFactory<T, ST = T> {
    status: DropdownStatus
    options: (DropdownItemProps & {_obj:ST|null})[]
    errorValue?: ST
    onSearchChange?: (evt: React.SyntheticEvent<HTMLElement>, dropdown: DropdownOnSearchChangeData) => void;
    searchFilter?: (options: DropdownItemProps[], query: string) => DropdownItemProps[]
    renderItem: (value?: ST) => DropdownItemProps
    validator:Validator<T>
    onDropdownOpen: (event: React.SyntheticEvent<HTMLElement>, data: DropdownProps) => void
    values: ST[],
    focused: boolean;
    changed: boolean;
    message: string | undefined;
    validate: (value: T | null | undefined, force?: boolean) => void
}

function toOptions<T>(values: T[], renderItem: (value?: T) => DropdownItemProps):(DropdownItemProps & {_obj:T})[] {
    return values.filter(obj => !isStateTreeNode(obj) || isAlive(obj)).map(obj => {
        let v = renderItem(obj);

        return {...v, _obj: isObservable(obj) ? toJS(obj) : obj}
    })
}

export class DropdownModelFactoryBase<T, ST = T> {

    status: DropdownStatus
    values: ST[]
    errorValue?: ST
    onSearchChange?: (evt: React.SyntheticEvent<HTMLElement>, dropdown: DropdownOnSearchChangeData) => void
    searchFilter?: (options: DropdownItemProps[], query: string) => DropdownItemProps[];
    focused: boolean = false;
    changed: boolean = false;
    message: string | undefined = undefined;

    getOptions():(DropdownItemProps & {_obj:ST|null})[] {
        if (this.status === "loading_error") {
            return [{text: this.errorLoadingMessage, _obj:null}]
        }

        return toOptions(this.values, this.renderItem);
    }

    constructor(private load: (valueKey?: string) => (Promise<[ST[], DropdownStatus]> | undefined),
                private renderItemFunction: (value?: ST) => DropdownItemProps,
                private errorLoadingMessage: string = "Error loading values",
                public validator:Validator<T> = () => undefined)
    {
        this.values = []
        this.status = "initial"
        this.renderItem = renderItemFunction.bind(this)
        this.updateOptions = this.updateOptions.bind(this)
        this.updateOptionValuesAndStatus = this.updateOptionValuesAndStatus.bind(this)
        this.onDropdownOpen = this.onDropdownOpen.bind(this)
        this.loadOptions = this.loadOptions.bind(this)
        this.searchUsingPromise = this.searchUsingPromise.bind(this)
    }

    shouldInvokeValidator = () => {
        return this.changed && !this.focused;
    };

    validate = (value: T | null | undefined, force: boolean = false) => {
        if (this.shouldInvokeValidator() || force) {
            this.message = this.validator(value);
        }
    };

    renderItem(value?: ST):DropdownItemProps {
        return this.renderItemFunction(value)
    }

    updateOptions(value:any) {
        if (value === false) {
            this.updateOptionValuesAndStatus([], 'loaded')
        } else {
            let [items, loadingStatus] = value;
            this.updateOptionValuesAndStatus(items, loadingStatus)
        }
    };

    private updateOptionValuesAndStatus(items: ST[], loadingStatus:DropdownStatus) {
        switch (loadingStatus) {
            case "loaded":
                this.values = items;
                this.status = "loaded";
                break;
            case "blocked":
                this.values = [];
                this.status = "blocked";
                break;
            case "error":
                this.values = [];
                this.status = "error"

        }
    }

    onDropdownOpen(event: React.SyntheticEvent<HTMLElement>, data: DropdownProps) {
        let value = data.value;
        this.loadOptions(value ? value.toString() : undefined);
    };

    loadOptions(valueKey?: string): (Promise<[ST[], DropdownStatus]> | undefined) {
        this.status = "loading";
        let mayBePromise = this.load(valueKey);
        let values = this.values;
        let promise: Promise<[ST[], DropdownStatus]> = mayBePromise
            ? mayBePromise
            : values
                ? Promise.resolve([values, "loaded"] as [ST[], DropdownStatus])
                : Promise.resolve(emptyStatus as [ST[], DropdownStatus]);
        return promise
            .then(this.updateOptions)
            .catch((error) => {
                this.status = "error";
                if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
                    console.log(error)
                }
                return error;
            })
    };

    searchUsingPromise(promise: (searchQuery: string) => (Promise<ST[]> | undefined)) {
        const self = this;
        return (evt: React.SyntheticEvent<HTMLElement>, data: DropdownOnSearchChangeData) => {
            if (data.searchQuery) {
                self.status = "loading";
                let p = promise(data.searchQuery);
                if (p) {
                    p.then(values => {
                        self.values = values;
                        self.status = "loaded"
                    }).catch(() => {
                        self.status = "error"
                    })
                } else {
                    self.values = [];
                    self.status = "loaded"
                }
            } else {
                self.values = [];
                self.status = "loaded"
            }

        }
    }
}

export class DropdownModel<T> implements IDropdownModel<T> {

    constructor(
        private factory: IDropdownModelFactory<T>,
        private x: (T | undefined | null) = null,
        private onValueChanged: (value: (T | undefined), dropdown: DropdownProps) => void = () => {}) {
    }

    get value() {
        return this.x
    }

    get options() {
        return this.factory.options
    }

    get status() {
        return this.factory.status
    }

    get errorValue() {
        return this.factory.errorValue
    }

    get message() {
        return this.factory.message;
    }

    validate() {
        this.factory.validate(this.x, true);
    }

    renderItem(value:T|undefined) {
        return this.factory.renderItem(value)
    }

    onChange = (evt:React.SyntheticEvent<HTMLElement>, dropdown: DropdownProps) => {

        let option = dropdown && dropdown.options && dropdown.options.find(option => option.value === dropdown.value);

        let value = option ? option._obj : undefined;

        this.onValueChanged(value, dropdown);
        this.factory.changed = true;
        this.factory.validate(this.x);
    };

    onDropdownOpen = (event: React.SyntheticEvent<HTMLElement>, data: DropdownProps) => {
        this.factory.onDropdownOpen(event, data)
    };

    onDropdownClose = () => {

        const value = this.value;
        this.factory.values = value ? [value] : []
    };

    onSearchChange = (evt: React.SyntheticEvent<HTMLElement>, dropdown: DropdownOnSearchChangeData) => {
        this.factory.onSearchChange && this.factory.onSearchChange(evt, dropdown)
    };

    searchFilter = (options: DropdownItemProps[], query: string) => {
        return (this.factory.searchFilter && this.factory.searchFilter(options, query)) || []
    };

    onBlur = (evt: React.SyntheticEvent<HTMLElement>, dropdown: DropdownProps) => {
        this.factory.focused = false;
        this.factory.validate(this.x);
    };

    onFocus = (evt: React.SyntheticEvent<HTMLElement>, dropdown: DropdownProps) => {
        this.factory.focused = true;
        this.factory.validate(this.x);
    };
}

export interface DropdownModelConstructor<T> {
    new (factory: IDropdownModelFactory<T>, x: (T | undefined | null), onValueChanged: (value: (T | undefined), dropdown: DropdownProps)=> void): DropdownModel<T>;
}

export class DropdownModelFactory<T> extends DropdownModelFactoryBase<T, T> implements IDropdownModelFactory<T, T>{

    constructor(load: (valueKey?: string) => (Promise<[T[], DropdownStatus]> | undefined),
                renderItemFunction: (value?: T) => DropdownItemProps,
                errorLoadingMessage: string = "Error loading values",
                validator:Validator<T> = () => undefined) {
        super(load, renderItemFunction, errorLoadingMessage,validator);
    }

    connect = (x: T | undefined | null, onValueChanged: (value: T | undefined, dropdown: DropdownProps) => void): IDropdownModel<T, T> => {
        return new DropdownModel(this, x, onValueChanged)
    };

    get options() {
        return super.getOptions();
    }
}

decorate(DropdownModelFactory, {
    status: observable,
    options: computed,
    message: observable,
    connect: action,
    validate: action
});

type PrimitiveOrUndefined = string | number | boolean | undefined;

function groupBy<V,K>(list:V[], keyGetter:(v:V) =>K):Map<K, V> {
    const map = new Map<K, V>();
    list.forEach((item) => {
        const key = keyGetter(item);
        if (key) {
            map.set(key, item);
        }
    });
    return map;
}

class DropdownMultiModel<T> implements IDropdownMultiModel<T> {

    private domainToDropdownItems = new Map<T, PrimitiveOrUndefined>();

    constructor(
        private factory: IDropdownModelFactory<T[], T>,
        private x: T[] = [],
        private onAddItems?: (value: T[], dropdown: DropdownProps) => void,
        private onRemoveItems?: (value: T[], dropdown: DropdownProps) => void,
    ) {
        this.renderItem = this.renderItem.bind(this)
    }

    get value() {
        return this.x
    }

    get options() {
        return (this.status === "loaded")
            ? this.factory.options
            // if options were not loaded use current value as available options... other options will be loaded onOpen
            : toOptions(this.value, this.renderItem);
    }

    get status() {
        return this.factory.status
    }

    get errorValue() {
        return this.factory.errorValue
    }

    get message() {
        return this.factory.message;
    }

    validate() {
        this.factory.validate(this.x, true);
    }

    renderItem(value?: T) {
        let dropdownItemProps = this.factory.renderItem(value)
        let dropdownItemValue = dropdownItemProps.value
        if (value) {
            this.domainToDropdownItems.set(value, dropdownItemValue)
        }
        return dropdownItemProps
    }

    onChange = (evt: React.SyntheticEvent<HTMLElement>, dropdown: DropdownProps) => {
        if (this.onAddItems || this.onRemoveItems) {
            const newValues = dropdown.value as (PrimitiveOrUndefined)[] || [];
            const options:DropdownItemProps[] = dropdown.options || [];
            const optionsByValue:Map<PrimitiveOrUndefined, DropdownItemProps> = groupBy(options, option => option.value || "");

            if (this.onAddItems) {
                let stringValueSet: Set<PrimitiveOrUndefined> = new Set<PrimitiveOrUndefined>();
                this.value.forEach(x => {
                    if (this.domainToDropdownItems.has(x)) {
                        stringValueSet.add(this.domainToDropdownItems.get(x))
                    }
                });
                let addedValues = newValues.filter(x => !stringValueSet.has(x as string));
                let valueArray = addedValues.map(value => optionsByValue.get(value)).map(val => val ? val._obj : undefined);
                this.onAddItems(valueArray, dropdown);
            }
            if (this.onRemoveItems) {
                let removedValues:(PrimitiveOrUndefined)[] = [];
                optionsByValue.forEach((value, key) => {if (!newValues.includes(key)) removedValues.push(key)});

                let valueArray = removedValues.map(value => optionsByValue.get(value)).map(val => val ? val._obj : undefined);
                this.onRemoveItems(valueArray, dropdown);
                valueArray.forEach(value => {
                    this.domainToDropdownItems.delete(value)
                })
            }
        }

        this.factory.changed = true;
        this.factory.validate(this.x);
    };

    onAdd = (evt: React.SyntheticEvent<HTMLElement>, option: DropdownItemProps) => {
        this.onAddItems && this.onAddItems([option._obj], {options: this.options} as DropdownProps);
        this.factory.changed = true;
        this.factory.validate(this.x);
    };

    onRemove = (evt: React.SyntheticEvent<HTMLElement>, option: DropdownItemProps) => {
        this.onRemoveItems && this.onRemoveItems([option._obj], {options: this.options} as DropdownProps);
        this.factory.changed = true;
        this.factory.validate(this.x);
    };

    onDropdownOpen = (event: React.SyntheticEvent<HTMLElement>, data: DropdownProps) => {
        this.factory.onDropdownOpen(event, data)
    };

    onDropdownClose = (event: React.SyntheticEvent<HTMLElement>, data: DropdownProps) => {
        if (this.value) {
            this.factory.values = this.value
        }
    };

    onSearchChange = (evt: React.SyntheticEvent<HTMLElement>, dropdown: DropdownOnSearchChangeData) => {
        this.factory.onSearchChange && this.factory.onSearchChange(evt, dropdown)
    };

    searchFilter = (options: DropdownItemProps[], query: string) => {
        return (this.factory.searchFilter && this.factory.searchFilter(options, query)) || []
    };

    onBlur = (evt: React.SyntheticEvent<HTMLElement>, dropdown: DropdownProps) => {
        this.factory.focused = false;
        this.factory.validate(this.x);
    };

    onFocus = (evt: React.SyntheticEvent<HTMLElement>, dropdown: DropdownProps) => {
        this.factory.focused = true;
        this.factory.validate(this.x);
    };
}

export interface DropdownMultiModelConstructor<T> {
    new(factory: IDropdownModelFactory<T[], T>, x: T[],
        onAdd?: (value: T[], dropdown: DropdownProps) => void,
        onRemove?: (value: T[], dropdown: DropdownProps) => void): DropdownMultiModel<T>;
}

export class DropdownMultiModelFactory<T> extends DropdownModelFactoryBase<T[], T>  implements IDropdownModelFactory<T[], T> {

    constructor(load: (valueKey?: string) => (Promise<[T[], DropdownStatus]> | undefined),
                renderItemFunction: (value?: T) => DropdownItemProps,
                errorLoadingMessage: string = "Error loading values",
                validator: Validator<T[]> = () => undefined) {
        super(load, renderItemFunction, errorLoadingMessage, validator);
    }

    connect = (x: T[] | undefined, onAddItems?: (value: T[], dropdown: DropdownProps) => void, onRemoveItems?: (value: T[], dropdown: DropdownProps) => void): IDropdownMultiModel<T> => {
        return new DropdownMultiModel(this, x, onAddItems, onRemoveItems);
    };

    get options() {
        return super.getOptions();
    }

}

decorate(DropdownMultiModelFactory, {
    options: computed,
    status: observable,
    message: observable,
    connect: action,
    validate: action
});
